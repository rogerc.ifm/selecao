import React from 'react'
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  ActivityIndicator
} from 'react-native'
import axios from 'axios'

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: '#c98b00s'
    },
    headerTintColor: '#fff'
  }

  constructor (props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  componentDidMount () {
    axios
      .get('http://dev.4all.com:3003/tarefa')
      .then(response => {
        this.setState({ list: response.data.lista, loading: false })
      })
      .catch(error => {
        console.log(error)
      })
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={[styles.containerLoad, styles.horizontalLoad]}>
          <ActivityIndicator size='large' color='#c98b00' />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.contentContainer}>
          <Text style={{ marginBottom: 10 }}>ESCOLHA UMA OPÇÃO:</Text>

          {this.state.list.map(entry => {
            return (
              <View
                key={entry + Math.random()}
                style={{ margin: 10, width: '100%', paddoing: 20 }}
              >
                <Button
                  onPress={() => {
                    this.props.navigation.navigate('Detail', {
                      data: entry
                    })
                  }}
                  title={entry}
                  color='#c98b00'
                  accessibilityLabel={entry}
                />
              </View>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center'
  },
  contentContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)'
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center'
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center'
  },
  navigationFilename: {
    marginTop: 5
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center'
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7'
  }
})
