import React from 'react'
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
  Alert,
  Button
} from 'react-native'

import { Linking } from 'react-native'
import { MapView } from 'expo'

import axios from 'axios'
import Layout from '../constants/Layout'
import IconBtn from '../components/IconBtn'
import Colors from '../constants/Colors'
import Coment from '../components/Coment'

export default class Detail extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title || '',
    headerTitleStyle: { textAlign: 'center', alignSelf: 'center' },
    headerStyle: {
      backgroundColor: Colors.tintColor,
      fontSize: 10
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold'
    },
    headerRight: (
      <Image
        style={{
          width: 20,
          height: 20,
          marginRight: 10
        }}
        source={require('../assets/images/pes.png')}
      />
    )
  })

  constructor (props) {
    super(props)
    this.state = {
      list: [],
      coments: [],
      loading: true,
      data: {}
    }
  }

  // Define o Titulo do Header apos a chamada ao endPoint, trocando o titulo de forma dinâmica
  changeTitle = titleText => {
    const { setParams } = this.props.navigation
    setParams({ title: titleText })
  }

  componentDidMount () {
    // Print component dimensions to console

    const { params } = this.props.navigation.state
    path = params.data
    axios
      .get('http://dev.4all.com:3003/tarefa/' + params.data)
      .then(response => {
        this.setState({
          data: response.data,
          loading: false,
          coments: response.data.comentarios
        })
        this.props.navigation.setParams({ title: response.data.endereco })
      })
      .catch(error => {
        console.log(error)
        Alert.alert(
          'Erro',
          'Algum problema ocorreu ao recuperar dados do servidor',
          [
            {
              text: 'OK',
              onPress: () => this.props.navigation.navigate('Home')
            }
          ],
          { cancelable: false }
        )
      })
  }

  render () {
    // Objeto default para montar o mapa
    const region = {
      latitude: this.state.data.latitude || 0,
      longitude: this.state.data.longitude || 0,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421
    }

    const coments = this.state.coments

    if (this.state.loading) {
      return (
        <View style={[styles.containerLoad, styles.horizontalLoad]}>
          <ActivityIndicator size='large' color={Colors.tintColor} />
        </View>
      )
    }

    return (
      <ScrollView
        ref='scrollView'
        style={{ flex: 1, backgroundColor: Colors.backgroundColor }}
      >
        <View style={styles.container}>
          <View style={{ flex: 1 }}>
            <Image
              style={{
                width: Layout.window.width - 20,
                height: Layout.window.width * 0.6
              }}
              source={{
                uri: this.state.data.urlFoto
              }}
            />
            <Text style={styles.title}>{this.state.data.cidade}</Text>
            <Image
              style={styles.banner}
              source={require('../assets/images/fav.png')}
            />
          </View>

          <View style={styles.containerBtns}>
            <View style={styles.rowBtn}>
              <IconBtn
                label='Ligar'
                onPress={() => {
                  Linking.openURL('tel:' + this.state.data.telefone)
                }}
              />
              <IconBtn
                label='Serviços'
                onPress={() => {
                  this.props.navigation.navigate('Service')
                }}
              />
              <IconBtn
                label='Endereço'
                onPress={() => {
                  Alert.alert(this.state.data.endereco)
                }}
              />
              <IconBtn
                label='Comentários'
                onPress={() => {
                  this.refs.scrollView.scrollToEnd({ animated: true })
                }}
              />
              <IconBtn
                label='Favoritos'
                onPress={() => {
                  console.log('Favoritos')
                }}
              />
            </View>
            <View
              style={{
                borderTopWidth: 1,
                borderTopColor: '#d3d3d3',
                marginTop: 5
              }}
            >
              <Text
                style={{
                  color: '#c98b00',
                  paddingTop: 10,
                  fontSize: 14
                }}
              >
                {this.state.data.texto}
              </Text>
            </View>
          </View>
          <View />

          <View
            style={{
              flex: 1,
              width: Layout.window.width - 20,
              height: 110,
              backgroundColor: '#c98b00'
            }}
          >
            <Image
              style={styles.pin}
              source={require('../assets/images/endereco.png')}
            />
            <View>
              <MapView style={styles.map} region={region}>
                <MapView.Marker
                  coordinate={region}
                  title={this.state.data.endereco}
                />
              </MapView>
            </View>
            <View
              style={{
                flex: 1,
                paddingRight: 40
              }}
            >
              <Text style={styles.address}>{this.state.data.endereco}</Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              width: Layout.window.width
            }}
          >
            <View style={{ flex: 1 }}>
              {// Foi improvisado esse key o ideal seria que do endpoint viesse um id único
                coments.map(entry => (
                  <Coment
                    key={entry.nome + Math.random()}
                    nome={entry.nome}
                    titulo={entry.titulo}
                    nota={entry.nota || 1}
                    foto={entry.urlFoto}
                    comentario={entry.comentario}
                  />
                ))}
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

// Abaixo estão definidos os componentes de estilo relativos a essa página
// Se existisse outras páginas com o mesmo layout poderia ser disponibilizado como modulo
const containerBtnsWidth = Layout.window.width - 70
const styles = StyleSheet.create({
  banner: {
    width: 80,
    height: 80,
    position: 'absolute',
    top: Layout.window.width * 0.6 - 40,
    right: 20,
    backgroundColor: '#fff',
    borderRadius: 40
  },

  address: {
    color: '#fff',
    fontSize: 11,
    textAlignVertical: 'center',
    textAlign: 'right',
    paddingRight: 20
  },

  pin: {
    width: 40,
    height: 40,
    position: 'absolute',
    top: 65,
    right: 10,
    backgroundColor: '#fff',
    borderRadius: 40
  },

  map: {
    width: '100%',
    height: 90,
    zIndex: -100,
    marginBottom: 2
  },

  title: {
    fontSize: 25,
    color: '#c98b00',
    marginTop: 10,
    paddingLeft: 10
  },

  btntxt: {
    color: '#c98b00',
    fontSize: 8
  },

  containerLoad: {
    flex: 1,
    justifyContent: 'center'
  },

  horizontalLoad: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },

  btn: {
    width: containerBtnsWidth / 5,
    height: containerBtnsWidth / 5
  },

  containerBtns: {
    flex: 1,
    marginTop: 20,
    padding: 10,
    width: Layout.window.width - 20,
    backgroundColor: '#fff'
  },

  rowBtn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Layout.window.width - 40
  },

  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(242,242,242,1)',
    flexDirection: 'column',
    paddingLeft: 10,
    paddingRight: 10
  }
})
