# Quero Transformar

### Requisitos

- Node
- Expo

## Instalação

`$ yarn install`

`$ yarn start`

## Feito Com

* [Expo](https://github.com/expo/expo) - The Expo platform for making cross-platform mobile apps 
* [Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
* [React Navigation](https://github.com/react-navigation/react-navigation) - Routing and navigation for your React Native apps

## Autores

* **Roger Cruz** - *Initial work* - [RogeerCruz](https://github.com/rogeercruz)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


