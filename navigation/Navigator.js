import React from 'react'
import { createStackNavigator } from 'react-navigation'
import HomeScreen from '../screens/HomeScreen'
import Detail from '../screens/Detail'
import Service from '../screens/Service'

//Define a Navegação e todas as páginas são "Filhas de HomeStack"
const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: () => ({
      title: `Lista`,
      headerBackTitle: null
    })
  },

  Detail: {
    screen: Detail,
    navigationOptions: () => ({
      headerBackTitle: null
    })
  },

  Service: {
    screen: Service,
    navigationOptions: () => ({
      title: `Serviços`,
      headerBackTitle: null
    })
  }
})

export default HomeStack
