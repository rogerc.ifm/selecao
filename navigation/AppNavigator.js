import React from 'react'
import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import Navigator from './Navigator'

export default createAppContainer(
  createSwitchNavigator({
    Main: Navigator
  })
)
