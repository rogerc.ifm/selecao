import React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native'
import PropTypes from 'prop-types'
import Stars from './Stars'
import Colors from '../constants/Colors'
import Layout from '../constants/Layout'

// Componente que Define o Card de comentários
export default class Coment extends React.Component {
  render () {
    const { nome, foto, nota, comentario, titulo } = this.props

    return (
      <View style={styles.starContent}>
        <View style={styles.card}>
          <Image
            style={styles.avatar}
            source={{
              uri: foto
            }}
          />
          <View style={styles.info}>
            <View style={styles.row}>
              <View style={styles.column}>
                <Text style={{ color: Colors.tintColor }}>{nome}</Text>
                <Text style={{ color: Colors.tintColor }}>{titulo}</Text>
              </View>
              <Stars number={nota} />
            </View>
            <Text style={{ color: Colors.tintColor }} numberOfLines={1}>
              {comentario}
            </Text>
          </View>
        </View>
      </View>
    )
  }
}

// Props do Component
Coment.propTypes = {
  nome: PropTypes.string,
  foto: PropTypes.string,
  nota: PropTypes.number,
  comentario: PropTypes.string,
  titulo: PropTypes.string
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    width: Layout.window.width - 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: 2,
    backgroundColor: '#fff',
    padding: 11
  },

  info: {
    flex: 3,
    flexDirection: 'column'
  },

  row: {
    flex: 3,
    justifyContent: 'space-between',
    flexDirection: 'row',
    overflow: 'hidden'
  },

  column: {
    flex: 3,
    justifyContent: 'space-between',
    flexDirection: 'column'
  },

  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 20
  },

  img: {
    width: 13,
    height: 13,
    marginLeft: 1
  }
})
