import React from "react";
import {
  StyleSheet,
  TouchableHighlight,
  View,
  Image,
  Text
} from "react-native";

import Colors from "../constants/Colors";
import Layout from "../constants/Layout";
import PropTypes from "prop-types";

//Componente que retorna um botão de acordo com o nome do label
export default class IconBtn extends React.Component {
  getIcon(label) {
    switch (label) {
      case "Ligar":
        return (
          <Image
            style={styles.img}
            source={require("../assets/images/ligar.png")}
          />
        );
      case "Serviços":
        return (
          <Image
            style={styles.img}
            source={require("../assets/images/servicos.png")}
          />
        );
      case "Endereço":
        return (
          <Image
            style={styles.img}
            source={require("../assets/images/endereco.png")}
          />
        );
      case "Comentários":
      return (
        <Image
          style={styles.img}
          source={require("../assets/images/comentarios.png")}
        />
      );
      case "Favoritos":
      return (
        <Image
          style={styles.img}
          source={require("../assets/images/fav2.png")}
        />
      );
      
      default:
      return (
        <Image
          style={styles.img}
          source={require("../assets/images/ligar.png")}
        />
      );
    }
  }

  render() {
    const { onPress, label } = this.props;

    return (
      <TouchableHighlight
        underlayColor="#fff"
        style={styles.btn}
        onPress={onPress}
      >
        <View style={styles.btnContainer}>
          {this.getIcon(label)}
          <Text style={styles.btntxt}>{label}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

//Obrigatório passar a função para atribuir o press assim como o label
IconBtn.propTypes = {
  onPress: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired
};

const containerBtnsWidth = Layout.window.width - 70;

const styles = StyleSheet.create({
  img: {
    width: Layout.window.width / 10,
    height: Layout.window.width / 10
  },

  btnContainer: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  },

  btntxt: {
    color: Colors.tintColor,
    fontSize: 8
  },

  btn: {
    width: containerBtnsWidth / 5,
    height: containerBtnsWidth / 5
  }
});
