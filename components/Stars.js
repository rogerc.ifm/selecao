import React from 'react'
import { StyleSheet, View, Image } from 'react-native'
import { times } from 'lodash'
import PropTypes from 'prop-types'

// Componente que retorna estrelas referentes a nota atribuida no comentário
export default class Stars extends React.Component {
  render () {
    const { number } = this.props

    return (
      <View style={styles.starContent}>
        {times(number, index => {
          return (
            <Image
              key={index}
              style={styles.img}
              source={require('../assets/images/fav2.png')}
            />
          )
        })}
      </View>
    )
  }
}

// Obrigatório passar o valor pois através da função times ele será repetido
Stars.propTypes = {
  number: PropTypes.number
}

const styles = StyleSheet.create({
  starContent: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    right: 0,
    top: 2
  },

  img: {
    width: 13,
    height: 13,
    marginLeft: 1
  }
})
